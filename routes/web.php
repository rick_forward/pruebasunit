<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('/user', function (Illuminate\Http\Request $request) {
    $request = $request->all();
    $request['created'] = false;
    if( ! empty($request['name'])){
        if( $request['name'] == 'Sally'){
            return ['created' => true];
        }
    }
    return [];
});

Auth::routes();

Route::get('/home', 'HomeController@index');
